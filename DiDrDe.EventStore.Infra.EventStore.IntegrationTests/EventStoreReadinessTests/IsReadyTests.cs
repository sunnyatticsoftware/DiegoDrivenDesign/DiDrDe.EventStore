﻿using System.Threading.Tasks;
using Autofac;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using EventStore.ClientAPI;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.EventStoreReadinessTests
{
    public static class IsReadyTests
    {
        public class Given_An_Event_Store_Connection_That_Is_Started_When_IsReadyAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStoreReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();
                var eventStoreConnection = container.Resolve<IEventStoreConnection>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();
                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _sut = new EventStoreReadiness(eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Be_Ready()
            {
                _result.Should().BeTrue();
            }
        }

        public class Given_An_Event_Store_Connection_That_Is_Not_Started_When_IsReadyAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStoreReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();
                var eventStoreConnection = container.Resolve<IEventStoreConnection>();

                _sut = new EventStoreReadiness(eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Ready()
            {
                _result.Should().BeFalse();
            }
        }
    }
}