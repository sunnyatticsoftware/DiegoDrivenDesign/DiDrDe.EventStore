﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeSerializableEvents
{
    public class FakeSerializableEvent
    {
        public Identity AggregateIdentity { get; set; }
        public FakeInterfaceImplementation Foo { get; set; }
    }
}