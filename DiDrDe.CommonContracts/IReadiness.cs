﻿using System.Threading.Tasks;

namespace DiDrDe.CommonContracts
{
    public interface IReadiness
    {
        Task<bool> IsReady();
    }
}