﻿using System;

namespace DiDrDe.CommonContracts
{
    public class Identity
    {
        public Guid Value { get; }

        public Identity(Guid value)
        {
            Value = value;
        }

        protected bool Equals(Identity other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((Identity)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static Identity Create()
        {
            return new Identity(Guid.NewGuid());
        }

        [Obsolete]
        public static Identity Create(string guidValue) => new Identity(Guid.Parse(guidValue));

        public override string ToString()
        {
            return $"{Value}";
        }
    }
}