﻿namespace DiDrDe.CommonContracts
{
    public interface IDomainEvent
    {
        Identity AggregateIdentity { get; }
    }
}