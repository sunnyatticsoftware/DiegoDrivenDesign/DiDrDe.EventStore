﻿using System;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventStoreReadinessTests
{
    public static class IsReadyAsyncTests
    {
        public class Given_An_Event_Store_Connection_That_Is_Active_When_IsReadyAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStoreReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var eventStoreConnectionMock = new Mock<IEventStoreConnection>();
                eventStoreConnectionMock
                    .Setup(x => x.ReadEventAsync(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<bool>(), It.IsAny<UserCredentials>()))
                    .ReturnsAsync(It.IsAny<EventReadResult>());
                var eventStoreConnection = eventStoreConnectionMock.Object;
                _sut = new EventStoreReadiness(eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Return_True()
            {
                _result.Should().BeTrue();
            }
        }

        public class Given_An_Event_Store_Connection_That_Throws_Any_Exception_When_IsReadyAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStoreReadiness _sut;
            private bool _result;

            protected override void Given()
            {
                var eventStoreConnectionMock = new Mock<IEventStoreConnection>();
                eventStoreConnectionMock
                     .Setup(x => x.ReadEventAsync(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<bool>(), It.IsAny<UserCredentials>()))
                     .Throws(It.IsAny<Exception>());
                var eventStoreConnection = eventStoreConnectionMock.Object;
                _sut = new EventStoreReadiness(eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.IsReady();
            }

            [Fact]
            public void Then_It_Should_Return_False()
            {
                _result.Should().BeFalse();
            }
        }
    }
}