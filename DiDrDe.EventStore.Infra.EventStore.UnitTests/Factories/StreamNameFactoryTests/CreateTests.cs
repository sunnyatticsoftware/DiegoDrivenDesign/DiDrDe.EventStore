﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.StreamNameFactoryTests
{
    public static class CreateTests
    {
        public class Given_A_Guid_And_A_Type_When_Create
            : Given_When_Then_Test
        {
            private StreamNameFactory _sut;
            private Guid _id;
            private Type _type;
            private string _expectedResult;
            private string _result;

            protected override void Given()
            {
                _id = GuidGenerator.Create(1);
                _type = typeof(object);
                _expectedResult = "Object.00000001000000000000000000000000";
                _sut = new StreamNameFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_id, _type);
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Name()
            {
                _result.Should().NotBeNullOrEmpty();
            }

            [Fact]
            public void Then_It_Should_Return_A_Composition_Of_The_Type_Name_With_The_Id_Without_Spaces()
            {
                _result.Should().Be(_expectedResult);
            }
        }
    }
}