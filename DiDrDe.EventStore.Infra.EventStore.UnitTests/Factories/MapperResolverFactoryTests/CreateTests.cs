﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.MapperResolverFactoryTests
{
    public static class CreateTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private Dictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private MapperResolver _result;

            private FakeSerializableEvent Serialize(FakeDomainEvent serializableEvent)
            {
                return null;
            }

            private FakeDomainEvent Deserialize(FakeSerializableEvent domainEvent)
            {
                return null;
            }

            protected override void Given()
            {
                var key = typeof(FakeDomainEvent);
                var domainEventEventSerializerDeserializer =
                    DomainEventSerializerDeserializer.Create<FakeDomainEvent, FakeSerializableEvent>(Serialize, Deserialize);
                _domainEventSerializerDeserializers =
                    new Dictionary<Type, DomainEventSerializerDeserializer>
                    {
                        {key, domainEventEventSerializerDeserializer}
                    };
            }

            protected override void When()
            {
                _result = MapperResolverFactory.Create(_domainEventSerializerDeserializers);
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Serializer()
            {
                _result.Should().NotBeNull();
            }
        }
    }
}