﻿using DiDrDe.EventStore.Infra.EventStore.Factories;
using EventStore.ClientAPI;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.RetrievedEventFactoryTests
{
    public static class CreateTests
    {
        public class Given_A_ResolveEvent_When_Create
            : Given_When_Then_Test
        {
            private RetrievedEventFactory _sut;
            private ResolvedEvent _resolvedEvent;
            private RetrievedEvent _result;

            protected override void Given()
            {
                _resolvedEvent = new ResolvedEvent();
                _sut = new RetrievedEventFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_resolvedEvent);
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Result()
            {
                _result.Should().NotBeNull();
            }
        }
    }
}