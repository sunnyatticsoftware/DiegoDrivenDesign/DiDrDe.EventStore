﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.ExpectedVersionFactoryTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private ExpectedVersionFactory _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new ExpectedVersionFactory();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_IExpectedVersionFactory()
            {
                _sut.Should().BeAssignableTo<IExpectedVersionFactory>();
            }
        }
    }
}