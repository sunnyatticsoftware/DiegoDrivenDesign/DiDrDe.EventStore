﻿using EventStore.ClientAPI;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventStoreManagerTests
{
    public static class StopTests
    {
        public class Given_An_Event_Store_Connection_When_Stop
            : Given_When_Then_Test
        {
            private EventStoreManager _sut;
            private Mock<IEventStoreConnection> _eventStoreConnectionMock;

            protected override void Given()
            {
                _eventStoreConnectionMock = new Mock<IEventStoreConnection>();
                var eventStoreConnection = _eventStoreConnectionMock.Object;
                _sut = new EventStoreManager(eventStoreConnection);
            }

            protected override void When()
            {
                _sut.Stop();
            }

            [Fact]
            public void Then_It_Should_Close_The_Connection()
            {
                _eventStoreConnectionMock.Verify(x => x.Close(), Times.Once);
            }
        }
    }
}