﻿using System;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.IoCC.DomainEventSerializerDeserializerTests
{
    public static class CreateTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventSerializerDeserializer _sut;
            private Func<FakeDomainEvent, FakeSerializableEvent> _serializer;
            private Func<FakeSerializableEvent, FakeDomainEvent> _deserializer;
            private Type _expectedDomainEventType;
            private Type _expectedSerializableEventType;

            protected override void Given()
            {
                _serializer = Mock.Of<Func<FakeDomainEvent, FakeSerializableEvent>>();
                _deserializer = Mock.Of<Func<FakeSerializableEvent, FakeDomainEvent>>();
                _expectedDomainEventType = typeof(FakeDomainEvent);
                _expectedSerializableEventType = typeof(FakeSerializableEvent);
            }

            protected override void When()
            {
                _sut = DomainEventSerializerDeserializer.Create(_serializer, _deserializer);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Domain_Event_Type_As_The_Origin_Type_Of_Serializer()
            {
                _sut.DomainEventType.Should().Be(_expectedDomainEventType);
            }

            [Fact]
            public void Then_It_Should_Have_The_Serializable_Event_Type_As_The_Destination_Type_Of_Serializer()
            {
                _sut.SerializableEventType.Should().Be(_expectedSerializableEventType);
            }
        }

        public class Given_Null_Serializer_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventSerializerDeserializer _sut;
            private Func<FakeDomainEvent, FakeSerializableEvent> _serializer;
            private Func<FakeSerializableEvent, FakeDomainEvent> _deserializer;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _serializer = null;
                _deserializer = Mock.Of<Func<FakeSerializableEvent, FakeDomainEvent>>();
            }

            protected override void When()
            {
                try
                {
                    _sut = DomainEventSerializerDeserializer.Create(_serializer, _deserializer);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_Deserializer_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventSerializerDeserializer _sut;
            private Func<FakeDomainEvent, FakeSerializableEvent> _serializer;
            private Func<FakeSerializableEvent, FakeDomainEvent> _deserializer;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _serializer = Mock.Of<Func<FakeDomainEvent, FakeSerializableEvent>>();
                _deserializer = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = DomainEventSerializerDeserializer.Create(_serializer, _deserializer);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventSerializerDeserializer _sut;
            private Func<FakeDomainEvent, FakeSerializableEvent> _serializer;
            private Func<FakeSerializableEvent, FakeDomainEvent> _deserializer;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _serializer = null;
                _deserializer = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = DomainEventSerializerDeserializer.Create(_serializer, _deserializer);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}