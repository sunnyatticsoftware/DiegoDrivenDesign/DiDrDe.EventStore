﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.DomainEventsRetrieverTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventsRetriever _sut;
            private IDomainEventFactory _domainEventFactory;
            private IEventStoreRepository _eventStoreRepository;

            protected override void Given()
            {
                _domainEventFactory = Mock.Of<IDomainEventFactory>();
                _eventStoreRepository = Mock.Of<IEventStoreRepository>();
            }

            protected override void When()
            {
                _sut = new DomainEventsRetriever(_domainEventFactory, _eventStoreRepository);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IDomainEventsRetriever()
            {
                _sut.Should().BeAssignableTo<IDomainEventsRetriever>();
            }
        }

        public class Given_Null_DomainEventFactory_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventsRetriever _sut;
            private IDomainEventFactory _domainEventFactory;
            private IEventStoreRepository _eventStoreRepository;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _domainEventFactory = null;
                _eventStoreRepository = Mock.Of<IEventStoreRepository>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new DomainEventsRetriever(_domainEventFactory, _eventStoreRepository);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_EventStoreRepository_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventsRetriever _sut;
            private IDomainEventFactory _domainEventFactory;
            private IEventStoreRepository _eventStoreRepository;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _domainEventFactory = Mock.Of<IDomainEventFactory>(); ;
                _eventStoreRepository = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new DomainEventsRetriever(_domainEventFactory, _eventStoreRepository);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventsRetriever _sut;
            private IDomainEventFactory _domainEventFactory;
            private IEventStoreRepository _eventStoreRepository;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _domainEventFactory = null;
                _eventStoreRepository = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new DomainEventsRetriever(_domainEventFactory, _eventStoreRepository);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}