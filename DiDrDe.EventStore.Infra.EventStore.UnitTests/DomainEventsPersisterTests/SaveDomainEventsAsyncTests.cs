﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.DomainEventsPersisterTests
{
    public static class SaveDomainEventsAsyncTests
    {
        public class Given_Three_Domain_Events_And_An_Aggregate_When_SaveDomainEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private DomainEventsPersister _sut;
            private Mock<IEventStoreRepository> _eventStoreRepositoryMock;
            private Mock<IExpectedVersionFactory> _expectedVersionFactoryMock;
            private Mock<IAggregateRoot> _aggregateRootMock;
            private Mock<IEventDataFactory> _eventDataFactoryMock;
            private IAggregateRoot _aggregateRoot;
            private string _streamName;
            private int _aggregateRootVersion;
            private int _domainEventsCount;
            private IEnumerable<IDomainEvent> _domainEvents;
            private Exception _exception;

            protected override void Given()
            {
                _streamName = "foo";
                _eventStoreRepositoryMock = new Mock<IEventStoreRepository>();
                var eventStoreRepository = _eventStoreRepositoryMock.Object;
                _eventDataFactoryMock = new Mock<IEventDataFactory>();
                _eventDataFactoryMock
                    .Setup(x => x.Create(It.IsAny<IDomainEvent>(), It.IsAny<IAggregateRoot>()))
                    .Returns(default(EventData));
                var eventDataFactory = _eventDataFactoryMock.Object;

                _aggregateRootMock = new Mock<IAggregateRoot>();
                _aggregateRootVersion = 1;
                _aggregateRootMock
                    .Setup(x => x.Version)
                    .Returns(_aggregateRootVersion);

                var aggregateIdentity = new IdentityBuilder().Build();
                var eventOne = new FakeDomainEvent(aggregateIdentity);
                var eventTwo = new FakeDomainEvent(aggregateIdentity);
                var eventThree = new FakeDomainEvent(aggregateIdentity);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        eventOne,
                        eventTwo,
                        eventThree
                    };
                _domainEventsCount = 3;

                _expectedVersionFactoryMock = new Mock<IExpectedVersionFactory>();
                _expectedVersionFactoryMock
                    .Setup(x => x.Create(_aggregateRootVersion, _domainEventsCount))
                    .Returns(It.IsAny<long>());
                var expectedVersionFactory = _expectedVersionFactoryMock.Object;
                _aggregateRoot = _aggregateRootMock.Object;

                _sut =
                    new EventStorePersisterBuilder()
                        .WithEventStoreRepository(eventStoreRepository)
                        .WithEventDataFactory(eventDataFactory)
                        .WithExpectedVersionFactory(expectedVersionFactory)
                        .Build();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.SaveDomainEventsAsync(_streamName, _aggregateRoot, _domainEvents);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Create_Three_EventData()
            {
                _eventDataFactoryMock.Verify(x => x.Create(It.IsAny<IDomainEvent>(), It.IsAny<IAggregateRoot>()), Times.Exactly(3));
            }

            [Fact]
            public void Then_It_Should_Retrieve_The_Expected_Version_Number()
            {
                _expectedVersionFactoryMock.Verify(x => x.Create(_aggregateRootVersion, _domainEventsCount));
            }

            [Fact]
            public void Then_It_Should_Save_The_Events_In_The_Repository()
            {
                _eventStoreRepositoryMock
                    .Verify(x => x.Save(_streamName, It.IsAny<long>(), It.IsAny<IEnumerable<EventData>>()), Times.Once);
            }
        }

        public class Given_A_Problem_In_The_Event_Store_Repository_When_SaveEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private DomainEventsPersister _sut;
            private Mock<IEventStoreRepository> _eventStoreRepository;
            private string _streamName;
            private IAggregateRoot _aggregateRoot;
            private IEnumerable<IDomainEvent> _domainEvents;
            private CouldNotPersistEventsException _exception;

            protected override void Given()
            {
                _streamName = "foo";
                _eventStoreRepository = new Mock<IEventStoreRepository>();
                _eventStoreRepository
                    .Setup(x => x.Save(_streamName, It.IsAny<long>(), It.IsAny<IEnumerable<EventData>>()))
                    .Throws(It.IsAny<Exception>());
                var eventStoreConnection = _eventStoreRepository.Object;

                _aggregateRoot = Mock.Of<IAggregateRoot>();
                _domainEvents = new List<IDomainEvent>();

                _sut =
                    new EventStorePersisterBuilder()
                        .WithEventStoreRepository(eventStoreConnection)
                        .Build();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.SaveDomainEventsAsync(_streamName, _aggregateRoot, _domainEvents);
                }
                catch (CouldNotPersistEventsException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_A_CouldNotPersistEventsException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}