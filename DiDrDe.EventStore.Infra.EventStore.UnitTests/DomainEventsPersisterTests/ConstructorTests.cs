﻿using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.DomainEventsPersisterTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventsPersister _sut;
            private IEventDataFactory _eventDataFactory;
            private IExpectedVersionFactory _expectedVersionFactory;
            private IEventStoreRepository _eventStoreRepository;

            protected override void Given()
            {
                _eventDataFactory = Mock.Of<IEventDataFactory>();
                _expectedVersionFactory = Mock.Of<IExpectedVersionFactory>();
                _eventStoreRepository = Mock.Of<IEventStoreRepository>();
            }

            protected override void When()
            {
                _sut = new DomainEventsPersister(_eventDataFactory, _expectedVersionFactory, _eventStoreRepository);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IDomainEventsPersister()
            {
                _sut.Should().BeAssignableTo<IDomainEventsPersister>();
            }
        }
    }
}