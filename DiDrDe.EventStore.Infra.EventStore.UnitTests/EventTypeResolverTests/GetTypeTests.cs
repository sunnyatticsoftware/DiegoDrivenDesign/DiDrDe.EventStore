﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventTypeResolverTests
{
    public static class GetTypeTests
    {
        public class Given_A_Class_Name_When_GetType
            : Given_When_Then_Test
        {
            private EventTypeResolver _sut;
            private string _fakeDomainEventTypeName;
            private Type _result;
            private Type _expectedType;

            protected override void Given()
            {
                var fakeDomainEventType = typeof(FakeDomainEvent);
                _fakeDomainEventTypeName = fakeDomainEventType.Name;
                var eventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType
                    };

                _sut = new EventTypeResolver(eventTypes);

                _expectedType = typeof(FakeDomainEvent);
            }

            protected override void When()
            {
                _result = _sut.GetType(_fakeDomainEventTypeName);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Type()
            {
                _result.Should().Be(_expectedType);
            }
        }
    }
}