﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport
{
    public class FakeDomainEvent
        : IDomainEvent
    {
        public Identity AggregateIdentity { get; }

        public FakeDomainEvent(Identity aggregateIdentity)
        {
            AggregateIdentity = aggregateIdentity;
        }
    }
}