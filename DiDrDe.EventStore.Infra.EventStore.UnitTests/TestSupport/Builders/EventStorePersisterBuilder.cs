﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using Moq;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders
{
    public class EventStorePersisterBuilder
    {
        private IEventDataFactory _eventDataFactory;
        private IExpectedVersionFactory _expectedVersionFactory;
        private IEventStoreRepository _eventStoreRepository;

        public EventStorePersisterBuilder()
        {
            _eventDataFactory = Mock.Of<IEventDataFactory>();
            _expectedVersionFactory = Mock.Of<IExpectedVersionFactory>();
            _eventStoreRepository = Mock.Of<IEventStoreRepository>();
        }

        public EventStorePersisterBuilder WithEventDataFactory(IEventDataFactory eventDataFactory)
        {
            _eventDataFactory = eventDataFactory;
            return this;
        }

        public EventStorePersisterBuilder WithExpectedVersionFactory(IExpectedVersionFactory expectedVersionFactory)
        {
            _expectedVersionFactory = expectedVersionFactory;
            return this;
        }

        public EventStorePersisterBuilder WithEventStoreRepository(IEventStoreRepository eventStoreRepository)
        {
            _eventStoreRepository = eventStoreRepository;
            return this;
        }

        public DomainEventsPersister Build()
        {
            var eventStorePersister = new DomainEventsPersister(_eventDataFactory, _expectedVersionFactory, _eventStoreRepository);
            return eventStorePersister;
        }
    }
}