﻿using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using Moq;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders
{
    public class DomainEventFactoryBuilder
    {
        private IBytesConverter _bytesConverter;
        private IJsonConverter _jsonConverter;
        private IMapperResolver _mapperResolver;
        private IEventTypeResolver _typeResolver;

        public DomainEventFactoryBuilder()
        {
            _bytesConverter = Mock.Of<IBytesConverter>();
            _jsonConverter = Mock.Of<IJsonConverter>();
            _mapperResolver = Mock.Of<IMapperResolver>();
            _typeResolver = Mock.Of<IEventTypeResolver>();
        }

        public DomainEventFactoryBuilder WithBytesConverter(IBytesConverter bytesConverter)
        {
            _bytesConverter = bytesConverter;
            return this;
        }

        public DomainEventFactoryBuilder WithJsonConverter(IJsonConverter jsonConverter)
        {
            _jsonConverter = jsonConverter;
            return this;
        }

        public DomainEventFactoryBuilder WithMapperResolver(IMapperResolver mapperResolver)
        {
            _mapperResolver = mapperResolver;
            return this;
        }

        public DomainEventFactoryBuilder WithTypeResolver(IEventTypeResolver typeResolver)
        {
            _typeResolver = typeResolver;
            return this;
        }

        public DomainEventFactory Build()
        {
            var domainEventFactory =
                new DomainEventFactory(
                    _bytesConverter,
                    _jsonConverter,
                    _mapperResolver,
                    _typeResolver);
            return domainEventFactory;
        }
    }
}