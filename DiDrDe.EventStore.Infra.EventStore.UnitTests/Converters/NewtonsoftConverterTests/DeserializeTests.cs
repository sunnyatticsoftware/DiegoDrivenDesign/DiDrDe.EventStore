﻿using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.NewtonsoftConverterTests
{
    public static class DeserializeTests
    {
        public class Given_A_Json_As_String_When_Deserialize_Using_Generics
            : Given_When_Then_Test
        {
            private NewtonsoftConverter _sut;
            private string _source;
            private Foo _expectedResult;
            private Foo _result;

            private class Foo
            {
                public string Key1 { get; set; }
                public int Key2 { get; set; }
            }

            protected override void Given()
            {
                _source = "{\"Key1\":\"value1\",\"Key2\":3}";
                _sut = new NewtonsoftConverter();
                _expectedResult =
                    new Foo
                    {
                        Key1 = "value1",
                        Key2 = 3
                    };
            }

            protected override void When()
            {
                _result = _sut.Deserialize<Foo>(_source);
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Result()
            {
                _result.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Return_The_Correct_Object()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }

        public class Given_A_Json_As_String_When_Deserialize_Using_Explicit_Type
            : Given_When_Then_Test
        {
            private NewtonsoftConverter _sut;
            private string _source;
            private Foo _expectedResult;
            private object _result;

            private class Foo
            {
                public string Key1 { get; set; }
                public int Key2 { get; set; }
            }

            protected override void Given()
            {
                _source = "{\"Key1\":\"value1\",\"Key2\":3}";
                _sut = new NewtonsoftConverter();
                _expectedResult =
                    new Foo
                    {
                        Key1 = "value1",
                        Key2 = 3
                    };
            }

            protected override void When()
            {
                _result = _sut.Deserialize(_source, typeof(Foo));
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null()
            {
                _result.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Return_The_Correct_Object()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }
    }
}