﻿using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.NewtonsoftConverterTests
{
    public static class SerializeTests
    {
        public class Given_A_String_When_Serialize
            : Given_When_Then_Test
        {
            private NewtonsoftConverter _sut;
            private object _source;
            private string _result;
            private string _expectedResult;

            protected override void Given()
            {
                _source = "test";
                _expectedResult = "\"test\"";
                _sut = new NewtonsoftConverter();
            }

            protected override void When()
            {
                _result = _sut.Serialize(_source);
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null_Or_Empty()
            {
                _result.Should().NotBeNullOrEmpty();
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Json_String()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_A_Custom_Object_When_Serialize
            : Given_When_Then_Test
        {
            private class Foo
            {
                public string Key1 { get; set; }
                public int Key2 { get; set; }
            }

            private NewtonsoftConverter _sut;
            private Foo _source;
            private string _result;
            private string _expectedResult;

            protected override void Given()
            {
                _sut = new NewtonsoftConverter();
                _source =
                    new Foo
                    {
                        Key1 = "value1",
                        Key2 = 3
                    };

                _expectedResult = "{\"Key1\":\"value1\",\"Key2\":3}";
            }

            protected override void When()
            {
                _result = _sut.Serialize(_source);
            }

            [Fact]
            public void Then_It_Should_Return_A_JsonMessage_For_Custom_Object()
            {
                _result.Should().Be(_expectedResult);
            }
        }
    }
}