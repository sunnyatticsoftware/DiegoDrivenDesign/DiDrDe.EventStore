﻿using System.Text;
using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.BytesConverterTests
{
    public static class FromBytesTests
    {
        public class Given_A_Bytes_Utf8_Encoded_When_FromBytes
            : Given_When_Then_Test
        {
            private BytesConverter _sut;
            private Encoding _encoding;
            private byte[] _source;
            private string _result;
            private string _expectedResult;

            protected override void Given()
            {
                _encoding = Encoding.UTF8;
                _source = _encoding.GetBytes("test");
                _expectedResult = "test";
                _sut = new BytesConverter(_encoding);
            }

            protected override void When()
            {
                _result = _sut.FromBytes(_source);
            }


            [Fact]
            public void Then_It_Should_Return_The_Expected_String_Result()
            {
                _result.Should().Be(_expectedResult);
            }
        }
    }
}