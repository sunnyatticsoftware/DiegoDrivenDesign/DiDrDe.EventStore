﻿using System.Collections.Generic;
using System.Text;
using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.BytesConverterTests
{
    public static class ToBytesTests
    {
        public class Given_A_String_And_A_Utf8_When_ToBytes
            : Given_When_Then_Test
        {
            private BytesConverter _sut;
            private byte[] _result;
            private Encoding _encoding;
            private string _source;
            private byte[] _expectedUtf8Bytes;
            private byte[] _nonExpectedUtf32Bytes;

            protected override void Given()
            {
                _encoding = Encoding.UTF8;
                _source = "test";
                _expectedUtf8Bytes = _encoding.GetBytes(_source);
                _nonExpectedUtf32Bytes = Encoding.UTF32.GetBytes(_source);
                _sut = new BytesConverter(_encoding);
            }

            protected override void When()
            {
                _result = _sut.ToBytes(_source);
            }

            [Fact]
            public void Then_It_Should_Return_A_Collection_Of_Bytes()
            {
                _result.Should().BeAssignableTo<ICollection<byte>>();
            }

            [Fact]
            public void Then_It_Should_Be_The_Expected_Utf8_Bytes()
            {
                _result.Should().BeEquivalentTo(_expectedUtf8Bytes);
            }

            [Fact]
            public void Then_It_Should_Not_Be_An_Unexpected_Utf32_Bytes()
            {
                _result.Should().NotBeEquivalentTo(_nonExpectedUtf32Bytes);
            }
        }

        public class Given_A_String_And_A_Utf32_When_ToBytes
            : Given_When_Then_Test
        {
            private BytesConverter _sut;
            private byte[] _result;
            private Encoding _encoding;
            private string _source;
            private byte[] _expectedUtf32Bytes;
            private byte[] _nonExpectedUtf8Bytes;

            protected override void Given()
            {
                _encoding = Encoding.UTF32;
                _source = "test";
                _expectedUtf32Bytes = _encoding.GetBytes(_source);
                _nonExpectedUtf8Bytes = Encoding.UTF8.GetBytes(_source);
                _sut = new BytesConverter(_encoding);
            }

            protected override void When()
            {
                _result = _sut.ToBytes(_source);
            }

            [Fact]
            public void Then_It_Should_Return_A_Collection_Of_Bytes()
            {
                _result.Should().BeAssignableTo<ICollection<byte>>();
            }

            [Fact]
            public void Then_It_Should_Be_The_Expected_Utf32_Bytes()
            {
                _result.Should().BeEquivalentTo(_expectedUtf32Bytes);
            }

            [Fact]
            public void Then_It_Should_Not_Be_An_Unexpected_Utf8_Bytes()
            {
                _result.Should().NotBeEquivalentTo(_nonExpectedUtf8Bytes);
            }
        }
    }
}