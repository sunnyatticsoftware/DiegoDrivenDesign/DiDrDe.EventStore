﻿using System;
using System.Text;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.BytesConverterTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private BytesConverter _sut;
            private Encoding _encoding;

            protected override void Given()
            {
                _encoding = Encoding.Default;
                _sut = new BytesConverter(_encoding);
            }

            protected override void When()
            {
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IBytesConverter()
            {
                _sut.Should().BeAssignableTo<IBytesConverter>();
            }
        }

        public class Given_Null_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private BytesConverter _sut;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                try
                {
                    _sut = new BytesConverter(null);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            protected override void When()
            {
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}