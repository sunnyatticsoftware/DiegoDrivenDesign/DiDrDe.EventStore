﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IoCC;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class MapperResolver
        : IMapperResolver
    {
        private readonly IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;

        public MapperResolver(IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> domainEventSerializerDeserializers)
        {
            _domainEventSerializerDeserializers =
                domainEventSerializerDeserializers ?? throw new ArgumentNullException(nameof(domainEventSerializerDeserializers));
        }

        public Delegate GetSerializer(Type originType)
        {
            var domainEventSerializerDeserializer = GetDomainEventSerializerDeserializer(originType);
            if (domainEventSerializerDeserializer is null)
            {
                return DefaultDelegate();
            }
            var serializer = domainEventSerializerDeserializer.Serializer;
            return serializer;
        }

        public Delegate GetDeserializer(Type destinationType)
        {
            var domainEventSerializerDeserializer = GetDomainEventSerializerDeserializer(destinationType);
            if (domainEventSerializerDeserializer is null)
            {
                return DefaultDelegate();
            }
            var deserializer = domainEventSerializerDeserializer.Deserializer;
            return deserializer;
        }

        private static Delegate DefaultDelegate()
        {
            object Func(object source) => source;
            return (Func<object, object>)Func;
        }

        private DomainEventSerializerDeserializer GetDomainEventSerializerDeserializer(Type key)
        {
            _domainEventSerializerDeserializers.TryGetValue(key, out DomainEventSerializerDeserializer value);
            return value;
        }
    }
}