﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class DomainEventsPersister
        : IDomainEventsPersister
    {
        private readonly IEventDataFactory _eventDataFactory;
        private readonly IExpectedVersionFactory _expectedVersionFactory;
        private readonly IEventStoreRepository _eventStoreRepository;

        public DomainEventsPersister(
            IEventDataFactory eventDataFactory,
            IExpectedVersionFactory expectedVersionFactory,
            IEventStoreRepository eventStoreRepository)
        {
            _eventDataFactory = eventDataFactory ?? throw new ArgumentNullException(nameof(eventDataFactory));
            _expectedVersionFactory = expectedVersionFactory ?? throw new ArgumentNullException(nameof(expectedVersionFactory));
            _eventStoreRepository = eventStoreRepository ?? throw new ArgumentNullException(nameof(eventStoreRepository));
        }

        public async Task SaveDomainEventsAsync(string streamName, IAggregateRoot aggregateRoot, IEnumerable<IDomainEvent> domainEvents)
        {
            var eventsToPersist = domainEvents.ToList();
            var aggregateVersion = aggregateRoot.Version;
            var numberOfEvents = eventsToPersist.Count;
            var events =
                eventsToPersist
                    .Select(x => _eventDataFactory.Create(x, aggregateRoot))
                    .ToList();
            var expectedVersion = _expectedVersionFactory.Create(aggregateVersion, numberOfEvents);

            try
            {
                await _eventStoreRepository.Save(streamName, expectedVersion, events);
            }
            catch (Exception exception)
            {
                var message = exception.Message;
                throw new CouldNotPersistEventsException(message, exception);
            }
        }
    }
}