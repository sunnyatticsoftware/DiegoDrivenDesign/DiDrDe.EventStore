﻿using System;
using System.Text;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;

namespace DiDrDe.EventStore.Infra.EventStore.Converters
{
    public class BytesConverter
        : IBytesConverter
    {
        private readonly Encoding _encoding;

        public BytesConverter(Encoding encoding)
        {
            _encoding = encoding ?? throw new ArgumentNullException(nameof(encoding));
        }

        public byte[] ToBytes(string source)
        {
            var byteArray = _encoding.GetBytes(source);
            return byteArray;
        }

        public string FromBytes(byte[] bytes)
        {
            var value = _encoding.GetString(bytes);
            return value;
        }
    }
}