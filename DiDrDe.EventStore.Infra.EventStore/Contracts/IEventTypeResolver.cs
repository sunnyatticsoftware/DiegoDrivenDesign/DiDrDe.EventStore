﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IEventTypeResolver
    {
        Type GetType(string className);
    }
}