﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories
{
    public interface IEventStoreRepository
    {
        Task Save(string streamName, long expectedVersion, IEnumerable<EventData> events);
        Task<IEnumerable<IRetrievedEvent>> Get(string streamName, int start, int count, bool resolveLinksTo);
    }
}