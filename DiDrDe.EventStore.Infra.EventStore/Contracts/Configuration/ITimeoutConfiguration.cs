﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Configuration
{
    public interface ITimeoutConfiguration
    {
        TimeSpan TimeSpan { get; }
    }
}