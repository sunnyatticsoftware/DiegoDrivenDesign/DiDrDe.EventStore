﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IDomainEventsPersister
    {
        Task SaveDomainEventsAsync(string streamName, IAggregateRoot aggregateRoot, IEnumerable<IDomainEvent> domainEvents);
    }
}