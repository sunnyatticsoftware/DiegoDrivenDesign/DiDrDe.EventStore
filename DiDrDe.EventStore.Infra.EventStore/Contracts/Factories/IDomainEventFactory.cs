﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IDomainEventFactory
    {
        IDomainEvent Create(IRetrievedEvent retrievedEvent);
    }
}