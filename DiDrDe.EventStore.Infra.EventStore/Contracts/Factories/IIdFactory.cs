﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IIdFactory
    {
        Guid Create();
    }
}