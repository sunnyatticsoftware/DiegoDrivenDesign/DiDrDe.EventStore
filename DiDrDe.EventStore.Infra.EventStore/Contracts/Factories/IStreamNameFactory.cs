﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IStreamNameFactory
    {
        string Create(Guid aggregateId, Type source);
    }
}