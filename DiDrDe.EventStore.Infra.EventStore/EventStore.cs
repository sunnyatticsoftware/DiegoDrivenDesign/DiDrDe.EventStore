﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class EventStore
        : IEventStore
    {
        private readonly IStreamNameFactory _streamNameFactory;
        private readonly IDomainEventsRetriever _eventStoreRetriever;
        private readonly IDomainEventsPersister _eventStorePersister;

        public EventStore(
            IStreamNameFactory streamNameFactory,
            IDomainEventsRetriever eventStoreRetriever,
            IDomainEventsPersister eventStorePersister)
        {
            _streamNameFactory = streamNameFactory;
            _eventStoreRetriever = eventStoreRetriever;
            _eventStorePersister = eventStorePersister;
        }

        public async Task<IEnumerable<IDomainEvent>> GetEvents(Guid aggregateId, Type aggregateType)
        {
            var streamName = _streamNameFactory.Create(aggregateId, aggregateType);
            var domainEvents = await _eventStoreRetriever.GetDomainEventsAsync(streamName);
            return domainEvents;
        }

        public async Task Persist(IAggregateRoot aggregateRoot, IEnumerable<IDomainEvent> domainEvents)
        {
            var aggregateId = aggregateRoot.AggregateIdentity.Value;
            var source = aggregateRoot.GetType();
            var streamName = _streamNameFactory.Create(aggregateId, source);
            await _eventStorePersister.SaveDomainEventsAsync(streamName, aggregateRoot, domainEvents);
        }
    }
}