﻿using System;
using System.Collections.Generic;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class EventDataFactory
        : IEventDataFactory
    {
        private readonly IIdFactory _idFactory;
        private readonly IJsonConverter _jsonConverter;
        private readonly IBytesConverter _bytesConverter;
        private readonly IMapperResolver _mapperResolver;

        public EventDataFactory(
            IIdFactory idFactory,
            IJsonConverter jsonConverter,
            IBytesConverter bytesConverter,
            IMapperResolver mapperResolver)
        {
            _idFactory = idFactory ?? throw new ArgumentNullException(nameof(idFactory));
            _jsonConverter = jsonConverter ?? throw new ArgumentNullException(nameof(jsonConverter));
            _bytesConverter = bytesConverter ?? throw new ArgumentNullException(nameof(bytesConverter));
            _mapperResolver = mapperResolver ?? throw new ArgumentNullException(nameof(mapperResolver));
        }

        public EventData Create(IDomainEvent domainEvent, IAggregateRoot aggregateRoot)
        {
            var domainEventType = domainEvent.GetType();
            var domainEventTypeName = domainEventType.Name;
            var mapper = _mapperResolver.GetSerializer(domainEventType);
            var serializableEvent = mapper.DynamicInvoke(domainEvent);
            var serializableEventType = serializableEvent.GetType();
            var serializableEventTypeName = serializableEventType.Name;
            var serializedMappedEvent = _jsonConverter.Serialize(serializableEvent);
            var data = _bytesConverter.ToBytes(serializedMappedEvent);
            var id = _idFactory.Create();
            var aggregateRootType = aggregateRoot.GetType();
            var aggregateRootTypeName = aggregateRootType.Name;
            var metadataEventHeaders = GetMetadataEventHeaders(aggregateRootTypeName, domainEventTypeName, serializableEventTypeName);
            var serializedMetadataEventHeaders = _jsonConverter.Serialize(metadataEventHeaders);
            var metadata = _bytesConverter.ToBytes(serializedMetadataEventHeaders);
            var eventData = new EventData(id, domainEventTypeName, true, data, metadata);
            return eventData;
        }

        private IDictionary<string, string> GetMetadataEventHeaders(
            string aggregateRootTypeName,
            string domainEventTypeName,
            string serializableEventTypeName)
        {
            var eventHeaders =
                new Dictionary<string, string>
            {
                {EventStoreConstants.AggregateRootTypeHeader, aggregateRootTypeName},
                {EventStoreConstants.DomainEventTypeHeader, domainEventTypeName},
                {EventStoreConstants.SerializableEventTypeHeader, serializableEventTypeName}
            };

            return eventHeaders;
        }
    }
}