﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class RetrievedEventFactory
        : IRetrievedEventFactory
    {
        public RetrievedEvent Create(ResolvedEvent resolvedEvent)
        {
            var retrievedEvent = new RetrievedEvent(resolvedEvent);
            return retrievedEvent;
        }
    }
}