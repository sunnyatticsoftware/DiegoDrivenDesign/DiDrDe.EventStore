﻿using System;
using System.Collections.Generic;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class DomainEventFactory
        : IDomainEventFactory
    {
        private readonly IBytesConverter _bytesConverter;
        private readonly IJsonConverter _jsonConverter;
        private readonly IMapperResolver _mapperResolver;
        private readonly IEventTypeResolver _typeResolver;

        public DomainEventFactory(
            IBytesConverter bytesConverter,
            IJsonConverter jsonConverter,
            IMapperResolver mapperResolver,
            IEventTypeResolver typeResolver)
        {
            _bytesConverter = bytesConverter ?? throw new ArgumentNullException(nameof(bytesConverter));
            _jsonConverter = jsonConverter ?? throw new ArgumentNullException(nameof(jsonConverter));
            _mapperResolver = mapperResolver ?? throw new ArgumentNullException(nameof(mapperResolver));
            _typeResolver = typeResolver ?? throw new ArgumentNullException(nameof(typeResolver));
        }

        public IDomainEvent Create(IRetrievedEvent recordedEvent)
        {
            var eventMetadata = recordedEvent?.Metadata;
            var eventMetadataJson = _bytesConverter.FromBytes(eventMetadata);
            var metadata = _jsonConverter.Deserialize<Dictionary<string, string>>(eventMetadataJson);
            var serializedEventTypeName = metadata[EventStoreConstants.SerializableEventTypeHeader];
            var serializedEventType = _typeResolver.GetType(serializedEventTypeName);
            var domainEventTypeName = metadata[EventStoreConstants.DomainEventTypeHeader];
            var domainEventType = _typeResolver.GetType(domainEventTypeName);
            var eventData = recordedEvent?.Data;
            var eventDataJson = _bytesConverter.FromBytes(eventData);
            var deserializedEvent = _jsonConverter.Deserialize(eventDataJson, serializedEventType);
            var mapper = _mapperResolver.GetDeserializer(domainEventType);
            var mappedEvent = mapper?.DynamicInvoke(deserializedEvent);
            return (IDomainEvent)mappedEvent;
        }
    }
}