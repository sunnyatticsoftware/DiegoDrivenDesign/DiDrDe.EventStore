﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class IdFactory
        : IIdFactory
    {
        public Guid Create()
        {
            var id = Guid.NewGuid();
            return id;
        }
    }
}
