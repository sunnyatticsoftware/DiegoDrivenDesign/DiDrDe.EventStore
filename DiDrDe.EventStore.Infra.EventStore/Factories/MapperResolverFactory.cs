﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.IoCC;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public static class MapperResolverFactory
    {
        public static MapperResolver Create(
            IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> domainEventSerializerDeserializers)
        {
            if (domainEventSerializerDeserializers is null)
            {
                domainEventSerializerDeserializers = new Dictionary<Type, DomainEventSerializerDeserializer>();
            }
            var mapperResolver = new MapperResolver(domainEventSerializerDeserializers);
            return mapperResolver;
        }
    }
}
