﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.IoCC
{
    public class EventStoreOptions
    {
        public string ConnectionString { get; set; }
        public TimeSpan TimeoutTimeSpan { get; set; }
    }
}