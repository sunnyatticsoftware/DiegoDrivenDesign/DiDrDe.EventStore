﻿using System;
using System.Collections.Generic;
using System.Linq;
using DiDrDe.EventStore.Infra.EventStore.Contracts;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class EventTypeResolver
        : IEventTypeResolver
    {
        private readonly IEnumerable<Type> _eventTypes;

        public EventTypeResolver(IEnumerable<Type> eventTypes)
        {
            _eventTypes = eventTypes;
        }

        public Type GetType(string className)
        {
            var typeFound =
                _eventTypes
                    .FirstOrDefault(x => x.Name.Equals(className));
            if (typeFound is null)
            {
                throw new KeyNotFoundException($"Could not find any provided type for the class name {className}");
            }
            return typeFound;
        }

        public override bool Equals(object otherTypeResolver)
        {
            if (!(otherTypeResolver is EventTypeResolver typeResolver))
            {
                return false;
            }
            var areEqual = _eventTypes.SequenceEqual(typeResolver._eventTypes);
            return areEqual;
        }

        public override int GetHashCode()
        {
            var hashCode = 1313217667 + EqualityComparer<IEnumerable<Type>>.Default.GetHashCode(_eventTypes);
            return hashCode;
        }
    }
}