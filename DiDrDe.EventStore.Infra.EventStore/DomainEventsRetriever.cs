﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class DomainEventsRetriever
        : IDomainEventsRetriever
    {
        private readonly IDomainEventFactory _domainEventFactory;
        private readonly IEventStoreRepository _eventStoreRepository;

        public DomainEventsRetriever(
            IDomainEventFactory domainEventFactory,
            IEventStoreRepository eventStoreRepository)
        {
            _domainEventFactory = domainEventFactory ?? throw new ArgumentNullException(nameof(domainEventFactory));
            _eventStoreRepository = eventStoreRepository ?? throw new ArgumentNullException(nameof(eventStoreRepository));
        }

        public async Task<IEnumerable<IDomainEvent>> GetDomainEventsAsync(
            string streamName,
            int start = 0,
            int count = 4096,
            bool resolveLinksTo = false)
        {
            try
            {
                var retrievedEvents = await _eventStoreRepository.Get(streamName, start, count, resolveLinksTo);
                var domainEvents =
                    retrievedEvents
                        .Select(x => _domainEventFactory.Create(x))
                        .ToList();
                return domainEvents;
            }
            catch (Exception exception)
            {
                var exceptionMessage = exception.Message;
                throw new CouldNotRetrieveEventsException(exceptionMessage, exception);
            }
        }
    }
}