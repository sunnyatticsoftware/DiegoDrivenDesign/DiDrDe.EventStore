﻿using Autofac;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Sample.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DiDrDe.EventStore.Sample.EventPersister
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Event persister sample");
            var containerBuilder = new ContainerBuilder();
            containerBuilder
                .RegisterEventStore(
                    context =>
                    {
                        var eventStoreOptions =
                            new EventStoreOptions
                            {
                                ConnectionString = Constants.EventStoreSettings.ConnectionString,
                                TimeoutTimeSpan = TimeSpan.FromSeconds(Constants.EventStoreSettings.TimeoutSeconds)
                            };
                        return eventStoreOptions;
                    });
            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            PersistEvents(componentContext, Constants.TestConfiguration.NumberOfEvents,
                Constants.TestConfiguration.GapMilliseconds).GetAwaiter().GetResult();
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
        }

        private static async Task PersistEvents(IComponentContext componentContext, int numberEvents, int gapMilliSeconds)
        {
            var eventStore = componentContext.Resolve<IEventStore>();
            var eventStoreManager = componentContext.Resolve<IEventStoreManager>();
            await eventStoreManager.StartAsync();
            var aggregateId = Constants.TestConfiguration.AggregateId;
            var aggregateIdentity = new Identity(aggregateId);
            var aggregateRoot = new AggregateRoot(aggregateIdentity);

            for (int index = 0; index < numberEvents; index++)
            {
                Thread.Sleep(gapMilliSeconds);
                var eventRandomId = Guid.NewGuid();
                var eventIdentity = new Identity(eventRandomId);
                var eventToPersist = new SomethingHappened(eventIdentity);
                var eventsToPersist =
                    new List<IDomainEvent>
                    {
                        eventToPersist
                    };
                await Console.Out.WriteLineAsync($"Persisting {eventToPersist}");
                try
                {
                    var version = await GetCorrectAggregateVersionForTests(eventStore, aggregateId);
                    aggregateRoot.Version = version + eventsToPersist.Count;
                    await eventStore.Persist(aggregateRoot, eventsToPersist);
                    await Console.Out.WriteLineAsync($"Persisted! Current version is {aggregateRoot.Version} \n");
                }
                catch (Exception exception)
                {
                    await Console.Out.WriteLineAsync($"Error {exception}");
                }
            }
        }

        //this is a workaround, valid for one thread testing because testing concurrency is not the goal
        private static async Task<int> GetCorrectAggregateVersionForTests(IEventStore eventStore, Guid aggregateId)
        {
            var events = await eventStore.GetEvents(aggregateId, typeof(AggregateRoot));
            var eventsRetrieved = events.ToList();
            var currentVersionInEventStore = eventsRetrieved.Count;
            return currentVersionInEventStore;
        }
    }
}
