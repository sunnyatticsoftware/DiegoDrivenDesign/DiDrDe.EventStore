﻿using Autofac;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Sample.Common;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DiDrDe.EventStore.Sample.EventRetriever
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Event retriever sample");
            var containerBuilder = new ContainerBuilder();
            containerBuilder
                .RegisterEventStore(
                    context =>
                    {
                        var eventStoreOptions =
                            new EventStoreOptions
                            {
                                ConnectionString = Constants.EventStoreSettings.ConnectionString,
                                TimeoutTimeSpan = TimeSpan.FromSeconds(Constants.EventStoreSettings.TimeoutSeconds)
                            };
                        return eventStoreOptions;
                    });
            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            RetrieveEvents(componentContext, Constants.TestConfiguration.NumberOfEvents,
                Constants.TestConfiguration.GapMilliseconds + 1000).GetAwaiter().GetResult();
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
        }

        private static async Task RetrieveEvents(IComponentContext componentContext, int numberEvents, int gapMilliSeconds)
        {
            var eventStore = componentContext.Resolve<IEventStore>();
            var eventStoreManager = componentContext.Resolve<IEventStoreManager>();
            await eventStoreManager.StartAsync();
            var limit = numberEvents * gapMilliSeconds;

            for (int index = 0; index < limit; index++)
            {
                Thread.Sleep(gapMilliSeconds);
                Console.WriteLine($"\nGetting events again from stream");
                try
                {
                    var events = await eventStore.GetEvents(Constants.TestConfiguration.AggregateId, typeof(AggregateRoot));
                    var retrievedEvents = events.ToList();
                    Console.WriteLine($"Found {retrievedEvents.Count} events");
                    foreach (var domainEvent in retrievedEvents)
                    {
                        Console.WriteLine($"Retrieved {domainEvent}");
                    }
                }
                catch (Exception exception)
                {
                    await Console.Out.WriteLineAsync($"Error {exception}");
                }
            }
        }
    }
}
