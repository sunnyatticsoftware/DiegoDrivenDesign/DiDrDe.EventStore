# DiDrDe.EventStore

## Instructions:

1. Download a standalone Event Store server https://eventstore.org/downloads/win/EventStore-OSS-Win-v5.0.1.zip
2. Start the server locally with `C:\EventStore-OSS-Win-v5.0.1>EventStore.ClusterNode.exe --mem-db` to run it with in-memory persistance
3. Run all the unit and integration Tests in the solution
