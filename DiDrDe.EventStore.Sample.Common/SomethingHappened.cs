﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Sample.Common
{
    public class SomethingHappened
        : IDomainEvent
    {
        public Identity AggregateIdentity { get; }
        public SomethingHappened(Identity aggregateIdentity)
        {
            AggregateIdentity = aggregateIdentity;
        }

        public override string ToString()
        {
            return $"Event with Id {AggregateIdentity.Value}";
        }
    }
}
