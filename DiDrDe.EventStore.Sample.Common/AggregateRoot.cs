﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Sample.Common
{
    public class AggregateRoot
        : IAggregateRoot
    {
        public Identity AggregateIdentity { get; }
        public int Version { get; set; } = 0;

        public AggregateRoot(Identity aggregateIdentity)
        {
            AggregateIdentity = aggregateIdentity;
        }
    }
}
