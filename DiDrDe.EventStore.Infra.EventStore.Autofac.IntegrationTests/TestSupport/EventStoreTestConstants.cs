﻿namespace DiDrDe.EventStore.Infra.EventStore.Autofac.IntegrationTests.TestSupport
{
    public static class EventStoreTestConstants
    {
        public const string TestConnectionString = "ConnectTo=tcp://admin:changeit@127.0.0.1:1113; HeartBeatTimeout=500";
    }
}